/*
 * dn-bind="Name of the listener or {this}" (What to listen)
 * dn-on="click\keyup\enter\change" (When to trigger)
 * dn-url="http://urltosite/"
 * dn-data="col:val|col2:val"
 * dn-method="xml\json\jsonp:get\post:ResponseArray" (jsonp will remove HEADERS, its used for cross-region requests, DEFAULT is json:get, ResponseArray is used to locate the array, its required for XML though)
 *
 * dn-on-delay="MSEC" (Will affect dn-on="keyup\pulse"
 * {if|X.func|<>!=|Y.func} stuff {else} other stuff {endif}
 * {col:DefaultValue here if its null} (First instance will be used, if {col} is used before {col:Not known} the {col} default will be used first
 *
 *
 * */
/* PolyFillerz*/
if (!Array.isArray) {
    Array.isArray = function (arg) {
        return Object.prototype.toString.call(arg) === '[object Array]';
    };
}

jQuery.Decklyn = function (init) {
    "use strict";
    /* Global findz */
    var x_csrf_token = $$('input[name=_token]').val();
    /* Callbacks */
    var callbacks = init.callbacks;
    /* Add default callback */
    callbacks.default = function () {

    };
    /* LangUGaGE ;P */
    var text = {
        http_status_codes: {
            '100': 'Continue',
            '101': 'Switching Protocols',
            '102': 'Processing',
            '200': 'OK',
            '201': 'Created',
            '202': 'Accepted',
            '203': 'Non-Authoritative Information',
            '204': 'No Content',
            '205': 'Reset Content',
            '206': 'Partial Content',
            '207': 'Multi-Status',
            '300': 'Multiple Choices',
            '301': 'Moved Permanently',
            '302': 'Moved Temporarily',
            '303': 'See Other',
            '304': 'Not Modified',
            '305': 'Use Proxy',
            '307': 'Temporary Redirect',
            '400': 'Bad Request',
            '401': 'Unauthorized',
            '402': 'Payment Required',
            '403': 'Forbidden',
            '404': 'Not Found',
            '405': 'Method Not Allowed',
            '406': 'Not Acceptable',
            '407': 'Proxy Authentication Required',
            '408': 'Request Time-out',
            '409': 'Conflict',
            '410': 'Gone',
            '411': 'Length Required',
            '412': 'Precondition Failed',
            '413': 'Request Entity Too Large',
            '414': 'Request-URI Too Large',
            '415': 'Unsupported Media Type',
            '416': 'Requested Range Not Satisfiable',
            '417': 'Expectation Failed',
            '418': 'I\'m a teapot',
            '422': 'Unprocessable Entity',
            '423': 'Locked',
            '424': 'Failed Dependency',
            '425': 'Unordered Collection',
            '426': 'Upgrade Required',
            '428': 'Precondition Required',
            '429': 'Too Many Requests',
            '431': 'Request Header Fields Too Large',
            '500': 'Internal Server Error',
            '501': 'Not Implemented',
            '502': 'Bad Gateway',
            '503': 'Service Unavailable',
            '504': 'Gateway Time-out',
            '505': 'HTTP Version Not Supported',
            '506': 'Variant Also Negotiates',
            '507': 'Insufficient Storage',
            '509': 'Bandwidth Limit Exceeded',
            '510': 'Not Extended',
            '511': 'Network Authentication Required'
        },
        words: {
            delete_confirm: 'Är du säker?'
        }
    };

    /* Helper functions */
    var h = {
        ws: function (selector, type, value) {  /* Wrap Selector, if we gonna use some kind of selector in a different way*/
            value = (value !== undefined) ? '=' + value : '';
            switch (type) {
                case 'class':
                    return '.' + selector;
                    break;
                case 'attr':
                    return '[' + selector + value + ']';
                    break;
                case 'id':
                    return '[#' + selector + ']';
                    break;
            }
        },
        XMLtoJSON: function ($XML, responseArray) {

            if (!$XML) {
                return null;
                /* Power BreaK*/
            }
            /* Get the responseArray as parent  */
            var re_parent = new RegExp('<' + responseArray + '(?: |>)(.*?)<\/' + responseArray + '>', 'g');
            var _parentsResponse = $XML.documentElement.innerHTML.replace(consts.regex.newlines, '').preg_match_all(re_parent);

            var aRet = [];

            each(_parentsResponse, function (tagCollection) {
                var tags = tagCollection.preg_match_all(/<(.*?)>(.*?)<\/(.*?)>/gmi);
                var Obj = {};
                each(tags, function (tag) {
                    if (consts.bool.warning && tag[1] !== tag[3]) {
                        return console.warn('[ ! ! ! WARNING ! ! ! ] Uneven XML attributes at <' + tag[1] + '> ' + tag[2] + '</' + tag[3] + '>. Aborting current row');
                    }
                    if (consts.bool.warning && Obj[tag[1]]) {
                        return console.warn('[ ! ! ! WARNING ! ! ! ] Duplicate name for <' + tag[1] + '> ' + tag[2] + '</' + tag[3] + '>. Aborting current row');
                    }
                    Obj[tag[1]] = tag[2];
                });
                aRet.push(Obj);
            });

            /* Backup working Copy*/
            /*
             $($XML).find(responseArray).children().each(function () {
             $(this).children().each(function () {
             var Obj = {};
             Obj[this.tagName] = this.innerHTML;
             aRet.push(Obj);

             });

             });
             */

            return aRet;
        }
    };


    var consts = {
        ver: 1.0,
        bool: {
            warning: (init.opt) ? !!init.opt.warning : true,
            debug: (init.opt) ? !!init.opt.debug : false,
            sync: (init.opt) ? !!init.opt.sync : false
        },
        regex: {
            directives: /(?:\{)(?!if|else|endif)([a-z0-9_.:# \-åäöéáýúü]+)(?!=})/gmi, /* Find directives {Will Match inside here, and ignore if else endif}*/
            newlines: /\r?\n|\r/g, /* Find new lines*/
            template_variable_default: /:([^{]+)(?=})/gmi, /*  {name: Will Match inside here}*/
            template_statement: /\{if(?:\|([a-z0-9_.]{0,})\|([<>=!]{1,2})\|([a-z0-9_.]{0,}))}(.*?)(?:\{endif}|\{else}(.*?)\{endif})/gmi /* Match statement pattern {if|x|<!=|y>]} {else} {endif}*/
        }
    };

    /* misc cache / options*/
    var cache = {
        template_default_key_val: {},
        opt: {}
    };

    /* Class binding */
    var cls = {
        checkbox_switch: 'decklyn-checkbox-switch',
        destroy_item: 'decklyn-destroy-item'
    };

    /* Attribute binding */
    var dn = {
        bind: 'dn-bind',
        listen: 'dn-listen',

        callback: 'dn-callback',
        prevent_default: 'dn-prevent-default',
        inherit: 'dn-inherit', /* ONly performed as "onload" but copies from the inheritor*/
        silent: 'dn-silent', /* Does only execute the url and never return any data.*/

        method: 'dn-method', /* json:get\post*/
        url: 'dn-url',
        on: 'dn-on', /* click \ change \ keyup \delay */
        on_delay: 'dn-on-delay', /* only performed on pulse and keyup*/

        data: 'dn-data',
        data_default_null: 'dn-data-default-null'
    };

    /* THe parzing objecTZ*/
    var parse = {
        abort: false, /* Kommer aborta allt även olika dn-binds, endast abort fungerar såhär */
        working: false, /* TODO: kanske använda som en synkNINg?*/
        /* The main eventZ*/
        event: function ($binder, e) {
            if (e && $binder.attr(dn.prevent_default) !== 'false') {
                e.preventDefault();
            }

            /* Sync requests if option is enabled*/
            while (parse.working && consts.bool.sync) {

            }
            parse.working = true;

            var dn_bind = parse.dn.bind($binder);
            var $listener = parse.dn.listener(dn_bind, $binder);
            var silent = parse.dn.silent($binder);
            var url = parse.dn.url($binder, $listener);
            var data = parse.dn.data($binder, $listener);
            var action = parse.dn.method(silent, $binder);
            /* dn-method buts its an action really.. */
            var template = parse.misc.template.get(dn_bind, silent, 'Null/Undefined', $listener);
            var callback = parse.dn.callback($binder, $listener);

            if (consts.bool.debug) {
                console.log(' # # # Start debug # # #');
                console.log('Silent: ' + silent);
                console.log('url:' + url);
                console.log('Callback:');
                console.log(callback);
                console.log('Data:');
                console.log(data);
                console.log('Action:');
                console.log(action);
                console.log('Template: ');
                console.log(template);
                console.log(' # # # End debug # # #');
            }

            /* Check if we need to stop this one is ASYNC! */
            if (parse.abort) {
                parse.abort = false;
                return console.error('## 1 or more requests was aborted due to an error, the fatal-switch are asynchronous ##');
            }

            /* What we gonna do My nigga? */
            switch (action.dataType) {
                case 'xml':
                case 'json':
                case 'jsonp':
                    AJAX(action, data, url, template, dn_bind, callback, silent, $binder, $listener);
                    break;
                default:
                    parse.working = false;
                    return console.error('[ ! ! ! ERROR ! ! ! ] Unknown dataType "' + action.dataType + '" in ' + dn.method + '="' + action.dataType + ':' + action.method + ':' + action.responseArray + '". Aborting request');
            }

            parse.working = false;


        },
        /* IF ELSE ENDIF in ajax request loop only */
        statement: function (tmp_template, aStatement, oData, key) {
            each(aStatement, function (statement) {
                var statement_identifier, statement_identifier2;

                /* Look for the actual key we are going to use with the request AJax data */


                if (new RegExp(key).test(statement[1])) {
                    statement_identifier = 1;
                    statement_identifier2 = 3;
                } else if (new RegExp(key).test(statement[3])) {
                    statement_identifier = 3;
                    statement_identifier2 = 1;
                }
                /* Ingen else för det hade pajjat allt ;D*/

                if (statement_identifier) {
                    /* We found the value to check against the OBJ object.*/
                    var returnValue = "";

                    /* Translate for Ez */
                    var str = statement[0];
                    /* what we want to replace */
                    /* Convert to string so we can compare them*/

                    /* Check for if statement function ONLY ON STRING  */
                    var A;
                    var A_STATEMENT = statement[statement_identifier];

                    if (/\./g.test(A_STATEMENT) && typeof A_STATEMENT == 'string') {
                        var _tmp = A_STATEMENT.split('.');

                        switch (_tmp[1]) {
                            case 'length':
                                A = oData[_tmp[0]].length;
                                break;
                            default:
                                console.warn('[ ! ! ! WARNING ! ! ! ] Unknown if statement function "' + _tmp[1] + '" at "' + A_STATEMENT + '"');
                                break;
                        }
                    } else {
                        A = oData[statement[statement_identifier]]
                    }


                    var IS = statement[2];
                    var B = typeof A === 'number' ? parseInt(statement[statement_identifier2]) : statement[statement_identifier2];
                    /* If the A statement is a number, convert to int*/
                    var IF_TRUE = statement[4];
                    var IF_FALSE = typeof statement[5] !== 'undefined' ? statement[5] : returnValue;

                    if (consts.bool.debug) {
                        console.log('');
                        console.log(' # # # Statement debug # # # ');
                        console.log('IF "' + A + '" ' + IS + ' "' + B + '" (Matching ' + typeof A + ' against ' + typeof B + ')');
                        console.log('THEN return: ' + IF_TRUE);
                        console.log('ELSE return: ' + IF_FALSE);
                        console.log(' # # # Statemend debug END # # #');
                        console.log('');
                    }
                    /* Check IF ENDIF */
                    switch (IS) {
                        case '=':
                        case '==':
                            if (A === B) {
                                returnValue = IF_TRUE;
                                /* IF true */
                            } else {
                                returnValue = IF_FALSE;
                            }
                            break;
                        case '!':
                        case '!!':
                        case '<>':
                        case '><':
                        case '!=':
                        case '=!':
                            if (A !== B) {
                                returnValue = IF_TRUE;
                                /* IF true */
                            } else {
                                returnValue = IF_FALSE;
                                /* IF false*/
                            }
                            break;
                        case '=<':
                        case '<=':
                            if (A <= B) {
                                returnValue = IF_TRUE;
                                /* IF true */
                            } else {
                                returnValue = IF_FALSE;
                                /* IF false*/
                            }
                            break;
                        case '>=':
                        case '=>':
                            if (A >= B) {
                                returnValue = IF_TRUE;
                                /* IF true */
                            } else {
                                returnValue = IF_FALSE;
                                /* IF false*/
                            }
                            break;
                        case '>':
                            if (A > B) {
                                returnValue = IF_TRUE;
                                /* IF true */
                            } else {
                                returnValue = IF_FALSE;
                                /* IF false*/
                            }
                            break;
                        case '<':
                            if (A < B) {
                                returnValue = IF_TRUE;
                                /* IF true */
                            } else {
                                returnValue = IF_FALSE;
                                /* IF false*/
                            }
                            break;
                        default:
                            console.warn('[ ! ! ! WARNING ! ! ! ] Unknown attribute "' + IS + '" in "' + A + '|' + is + '|' + B + '"');
                            break;
                    }
                    /* Replace tempalte with "truth" We only use native REPLACE without the Global attribute, because with global it replaces the same shit 3 times for some reason */
                    tmp_template = tmp_template.replace(str, returnValue);
                }
            });
            return tmp_template;
        },
        /* {slector:opt1:opt2:opt3} */
        directives: function (sData, $binder, $listener) {
            var directives = {
                finisher: function (sName, $selector, A, B, C) {
                    B = (B ? B : null);
                    C = (C ? C : null);
                    var parent = this;

                    switch (A) {
                        case 'val':
                            return $selector.val();
                            break;
                        case 'text':
                            return $selector.text();
                            break;
                        case 'attr':
                            return $selector.attr(B);
                            break;
                        case 'checked':
                            /* Because each is scoped */
                            var $return = null;
                            $selector.each(function () {
                                if ($(this).is(':checked')) {
                                    return $return = parent.finisher(sName, $(this), B, C);
                                }
                            });
                            return $return;
                            break;
                        case 'selected':
                            return this.finisher(sName, $selector.find('option:selected'), B, C);
                        default:
                            if (consts.bool.warning) {
                                console.warn('[ ! ! ! WARNING ! ! ! ] {' + sName + ':' + A + '} "' + A + '" is not a valid finisher');
                            }
                            return null;
                    }
                },
                /* Use construct*/
                construct: function (sData, $binder, $listener) {
                    /* Find all  directives in the string */
                    var directives = sData.preg_match_all(consts.regex.directives);
                    /* Temp */
                    var sRet = sData;
                    /* get this */
                    var _this = this;
                    /* Loop shit */
                    each(directives, function (directive) {
                        var opt = directive.split(':');
                        var sName = opt.shift();
                        var $selector;

                        /*Get the selector jQuery object*/
                        switch (sName) {
                            case 'this':
                                $selector = $binder;
                                break;
                            case 'listener':
                                $selector = $listener;
                                break;
                            default:
                                /* Using cache*/
                                $selector = $$(sName);
                                if (!$selector.length) {
                                    return console.error('[ ! ! ! ERROR ! ! ! ] Cannot get $$("' + sName + '") because it does not exist in the scope (It might have been dynamicly added?), Aborting.');
                                }
                                break;
                        }
                        /* Check the length of the {...} and determine what is going to be uzed */
                        var value = null;
                        switch (opt.length) {
                            case 1:
                                value = _this.finisher(sName, $selector, opt[0]);
                                break; //
                            case 2:
                                value = _this.finisher(sName, $selector, opt[0], opt[1]);
                                break;
                            case 3:
                                value = _this.finisher(sName, $selector, opt[0], opt[1], opt[2]);
                                break;
                            default:
                                if (consts.bool.warning) {
                                    console.warn('[ ! ! ! WARNING ! ! ! ] {' + directive + '} "' + sName + '" could not be matched for a finisher. length: ' + opt.length);
                                }
                                break;
                        }

                        /* Throw warning for unkown values (will ignore empty strings) */
                        if (!value && value !== "") {
                            if (consts.bool.warning) {
                                console.warn('[ ! ! ! WARNING ! ! ! ] {' + directive + '} does not contain a value');
                            }
                        }
                        /* Replace ALL occurences of the directive. */
                        sRet = sRet.preg_replace('{' + directive + '}', value);

                    });
                    return sRet;
                }
            };
            return directives.construct(sData, $binder, $listener)
        },
        /* Dn attribute parsing*/
        dn: {
            bind: function ($binder) {
                var dn_bind = $binder.attr(dn.bind);

                if (dn_bind == '{this}') {
                    var this_id = 'this' + Math.round(Math.random() * 50000);
                    $binder.attr('dn-bind', this_id);
                    $binder.attr('dn-listen', this_id);
                    return this_id;
                }

                return dn_bind;
            },
            listener: function (dn_bind, $binder) {
                var attr_val = h.ws(dn.listen, 'attr', dn_bind);
                var $listener = (dn_bind === '{this}' ? $binder : $$(attr_val));

                if (!$listener.length || !$listener) {
                    console.error('[ ! ! ! ERROR ! ! ! ] could not find listener "' + attr_val + '". Aborting!');
                    return parse.abort = true;
                }

                return $listener;

            },
            silent: function ($binder) {
                return ($binder.attr(dn.silent) === 'true');
            },
            data: function ($binder, $listener) {
                /* Return empty object if no data is given*/
                var sData = $binder.attr(dn.data);

                if (!sData) {
                    return {};
                }
                /* Remove data spacing. (If any)*/
                sData = parse.directives(sData, $binder, $listener);
                var oData = {}, objName;

                eachSplit(sData, '|', function (row) {
                    var cols = row.split(':');

                    if (consts.bool.warning && row === "") {
                        console.warn('[ ! ! ! WARNING ! ! ! ] Empty data after "|" AT "' + sData + '"');
                        return oData;
                    }

                    if (cols.length < 2) {
                        if (consts.bool.warning) {
                            console.warn('[ ! ! ! WARNING ! ! ! ] Missing ":" after "' + cols + '" AT "' + sData + '"');
                        }
                        /* In case of error, stop parse and return valid data*/
                        return oData;
                    }
                    objName = cols.shift().replace(/\t|\s/g, '');
                    oData[objName] = cols.join(':');
                });

                return oData;
            },
            url: function ($binder, $listener) {
                var dn_url = $binder.attr(dn.url);

                if (!dn_url.length) {
                    console.error('[ ! ! ! ERROR ! ! ! ] could not find attribute ' + dn.url + '. Aborting!');
                    return parse.abort = true;
                }

                return parse.directives(dn_url, $binder, $listener)
            },
            method: function (silent, $binder) {
                var dn_method = $binder.attr(dn.method);

                if (!dn_method) {
                    console.error('[ ! ! ! ERROR ! ! ! ] could not find attribute ' + dn.method + '. Aborting!');
                    return parse.abort = true;
                }
                dn_method = dn_method.split(':');

                return {
                    dataType: !dn_method[0] ? 'json' : dn_method[0],
                    method: !dn_method[1] ? 'get' : dn_method[1],
                    responseArray: !dn_method[2] ? null : dn_method[2] /* Position of array, default is nothing*/
                };
            },
            callback: function ($binder, $listener) {
                /* Check for callback or use default AKA "none" */
                var dn_callback = $binder.attr(dn.callback);
                var oRet = {func: callbacks['default'], param: ''};

                if (dn_callback) {
                    var callback = dn_callback.split(':');
                    var name = callback.shift();
                    if (name.length && typeof callbacks[name] === 'function') {
                        var param = callback.join(':');
                        oRet = {
                            func: callbacks[name],
                            param: ((param) ? parse.directives(param, $binder, $listener) : '')
                        }
                    } else {
                        if (consts.bool.warning) {
                            console.warn('[ ! ! ! WARNING ! ! ! ] ' + dn.callback + '="' + dn_callback + '". Callback function "' + name + '". Does not exist and wont be executed, check your configurationZ.');
                        }
                    }
                }
                return oRet;
            }
        },
        misc: {
            template: {
                get: function (dn_bind, silent, dn_data_default_null, $listener) {
                    /* TODO: kanske måste ha unique_fast? testa sen*/
                    /* If we are silent, template will be null */
                    if (silent) {
                        return null;
                    }
                    if (!dn_bind) {
                        console.error('[ ! ! ! ERROR ! ! ! ] The attribute ' + dn.bind + ' is empty or does not exist. Aborting!');
                        return parse.abort = true;
                    }
                    /* Get template if it exists, defaults will already be set */
                    var template = $$$.exists(dn_bind + ':template');
                    /* Get any statement if its possible*/

                    if (template) {
                        return template;
                    }
                    /* Fetch template for the first time */
                    template = $listener.html();

                    /* Create storage for default values */
                    cache.template_default_key_val[dn_bind] = {};

                    var variables = template.preg_match_all(consts.regex.directives).getUnique();
                    var cur;
                    each(variables, function (val) {
                        cur = val.split(':');
                        /* Check for reserved directives (Used in other stuff)*/
                        /* Only check if same value hasnt been parsed yet*/
                        /* IGNORE MULTIPLE OCCURENCES */
                        if (!cache.template_default_key_val[dn_bind][cur[0]]) {
                            cache.template_default_key_val[dn_bind][cur[0]] = (cur.length > 1) ? cur[1] : dn_data_default_null;
                        }

                    });

                    /* Remove default values {a:DEfault here} and newlines and fix innerHTML aids with < and >*/
                    var html = template.replace(consts.regex.template_variable_default, '')
                        .replace(consts.regex.newlines, '')
                        .replace(/&gt;/gmi, '>')
                        .replace(/&lt;/gmi, '<');

                    return $$$.set(dn_bind + ':template', {
                        html: html,
                        statements: html.preg_match_all(consts.regex.template_statement)
                    });
                },
                getKeys: function (dn_bind) {
                    return cache.template_default_key_val[dn_bind];
                }
            }
        }
    };

    /* Yeah its only gon be ajax for now on x, XML and others will be defined aswell.)*/
    var AJAX = function (action, data, url, template, dn_bind, callback, silent, $binder, $listener) {
        var request = {
            headers: {'X-CSRF-TOKEN': x_csrf_token},
            crossDomain: true,
            method: action.method,
            dataType: action.dataType, /* Incase we go silent we dont expect any return*/
            data: data,
            url: url
        };

        /* Remove shit if jsonp*/
        if (action.dataType === 'jsonp') {
            delete request.headers;
        }

        $.ajax(request).complete(function (jqXHR) {

            var data;
            if (!silent) {
                switch (action.dataType) {
                    case 'json':
                    case 'jsonp':
                        data = jqXHR.responseJSON;
                        /* Convert Object Object to Object Array*/
                        break;
                    case 'xml':
                        data = h.XMLtoJSON(jqXHR.responseXML, action.responseArray);
                        break;
                }
            }
            if (consts.bool.debug) {
                console.log('# Actual jqXHR response START');
                console.log(data);
                console.log('# Actual jqXHR response END');
            }

            switch (jqXHR.status) {
                case 0: /* Might be Json P errorz*/
                    return console.error('[ ! ! ! ERROR ! ! ! ] Request was NOT successful, Request header fields is not allowed by Access-Control-Allow-Headers in preflight response. Try using "jsonp" instead of "json"');
                    break;
                case 200:

                    /* Noob controller */
                    if (!data && !silent && consts.bool.warning) {
                        console.warn('[ ! ! ! WARNING ! ! ! ] Request was successful but no data was returned. set "' + dn.silent + '" to true if no data is expected');
                    }

                    /* What object should we start collect data from? default is from the actual response.
                     * Silent will disable this check */
                    if (!silent) {
                        if (data && action.responseArray && !data[action.responseArray]) {
                            return console.error('[ ! ! ! ERROR ! ! ! ] Request was successful but the RESPONSE array "' + action.responseArray + '" could not be found..');
                        } else {
                            data = action.responseArray ? data[action.responseArray] : data;
                        }
                    }

                    if (!silent && !action.responseArray && action.dataType == 'xml') {
                        return console.error('[ ! ! ! ERROR ! ! ! ] Request was successful but the responseArray is missing, it is required for XML ajax requests, example: ' + dn.method + '="xml:x:ResponseArrayName" ');
                    }

                    if (!silent && !Array.isArray(data)) {
                        return console.error('[ ! ! ! ERROR ! ! ! ] Request was successful but the data was not an object array or an object, instead "' + data + '" was returned.');
                    }

                    /* IF silent we dont parse the data, just return it (!Should be here)*/
                    if (silent) {
                        if (consts.bool.debug) {
                            console.log('%c Ajax request was SUCCESSFUL', 'background: #222; color: #bada55');
                        }
                        return callback.func(true, jqXHR.status, text.http_status_codes[jqXHR.status], data, $binder, $listener, callback.param);
                    }

                    /* Get default values (if any) */
                    var templateDefaultValues = parse.misc.template.getKeys(dn_bind);

                    /* Clean prev result */
                    $listener.empty();
                    /* Our result */
                    var aRes = [];

                    /* Parse ARRAY */
                    each(data, function (oData) {
                        /* Copy original template fora new row*/
                        var tmp_template = template.html;
                        each(oData, function (key, value) {

                            /* Break if we dont need to parse the row */
                            if (!templateDefaultValues[key]) {
                                // Skip values that are not in the template
                            } else {
                                var val = !value ? templateDefaultValues[key] : value;

                                /* Check if we going to use a "IF statement" 8-|, and replace it before the actual data release */
                                if (template.statements.length) {
                                    tmp_template = parse.statement(tmp_template, template.statements, oData, key);
                                }
                                /* Do the regular Stuff (Cus {} can be used inside an statement */
                                tmp_template = tmp_template.preg_replace('{' + key + '}', val);
                            }
                        });

                        aRes.push(tmp_template);
                    });


                    /* Apply data */
                    $listener.html(aRes.join(''));

                    /* return callback*/
                    if (consts.bool.debug) {
                        console.log('%c Ajax request was SUCCESSFUL', 'background: #222; color: #bada55');
                    }
                    return callback.func(true, jqXHR.status, text.http_status_codes[jqXHR.status], data, $binder, $listener, callback.param);
                    break;
                default: /* Eveyrthing but 200 is considered failure*/
                    return callback.func(false, jqXHR.status, text.http_status_codes[jqXHR.status], null, $binder, $listener, callback.param);
                    break;
            }
        });
    };

    /* Misc debug */
    if (consts.bool.debug) {
        console.log('Decklyn Ver: ' + consts.ver);
        console.log('Debug: True');
        console.log('Warning enabled? ' + consts.bool.warning);
        console.log('Sync enabled? ' + consts.bool.sync);
    }

    /* Inheritor */
    var $inheritor = $(h.ws(dn.inherit, 'attr'));
    if ($inheritor.length) {
        $(h.ws(dn.bind, 'attr', $inheritor.attr(dn.inherit))).each(function () {
            var $parent_id = $(this).attr('id');

            if (!$parent_id && consts.bool.warning) {
                return console.warn('[ ! ! ! WARNING ! ! ! ] Failed to inherit "' + $inheritor.attr(dn.inherit) + '" because the id="" attribute is missing from the parent');
            }
            $.each(this.attributes, function () {
                if (this.specified) {
                    /* Copy all but dn-on to thjis element, replace this with parent id TODO: this one is risky*/
                    if (this.name !== dn.on && this.name !== 'id' && this.name.substring(0, 2) === 'dn') {
                        var value = this.value.preg_replace('{this', '{#' + $parent_id);
                        $inheritor.attr(this.name, value);
                    }
                }
            });
        });
    }

    /* Onclick */
    $(h.ws(dn.on, 'attr', 'click')).on('click', function (e) {
        return parse.event($(this), e);
    });

    /* On change */
    $(h.ws(dn.on, 'attr', 'change')).on('change', function (e) {
        return parse.event($(this), e);
    });

    /* on ENTER */
    $(h.ws(dn.on, 'attr', 'enter')).keypress(function (e) {
        if (e.which == 13) {
            return parse.event($(this), e)
        }
    });

    /* On keyup with optional delay */
    $(h.ws(dn.on, 'attr', 'keyup')).keyup(function (e) {
        var $this = $(this);
        var $e = e;
        var $this_delay = $this.attr(dn.on_delay);
        var delay = ($this_delay) ? $this_delay : 500;
        clearTimeout($.data(this, 'timer'));
        var wait = setTimeout(function () {
            return parse.event($this, $e);
        }, delay);
        $this.data('timer', wait);
    });

    /* On load */
    var $onLoad = $(h.ws(dn.on, 'attr', 'load'));
    if ($onLoad.length) {
        $onLoad.each(function () {
            parse.event($(this), null);
        });
    }

    /* On TICK. uses dn-delay*/
    var $onTick = $(h.ws(dn.on, 'attr', 'tick'));
    if ($onTick.length) {
        // Load initially
        $onTick.each(function () {
            parse.event($(this), null);
        });

        //Default delay "dn-on-delay"
        var $this_delay = $onTick.attr(dn.on_delay);
        var delay = ($this_delay) ? $this_delay : 500;
        //Add interval func
        setInterval(function () {
            $onTick.each(function () {
                parse.event($(this), null);
            });
        }, delay);
    }

    /* Regular decklyn remove */
    $(h.ws(cls.destroy_item, 'class')).on('click', function (e) {
        e.preventDefault();
        return confirm(text.words.delete_confirm) ? $('<form action="' + $(this).attr('href') + '" method="POST"><input name="_token" type="hidden" value="' + x_csrf_token + '"> <input name="_method" type="hidden" value="DELETE"></form>').submit() : false;
    });

    /* checkbox post-back validation error fix for BOOL values */
    $(h.ws(cls.checkbox_switch, 'class')).on('click', function () {
        return ($(this).val() === '1') ? $(this).val('0') : $(this).val('1');
    });
};